# perpustakaanv2

aplikasi ini merupakan sistem perpustakaan sederhana yang mempunyai fungsi :
 - melihat, menambah, mengubah, mendelete data customer
 - melihat, menambah, mengubah, mendelete data buku
 - melihat, menambah dan mengubah status peminjaman buku

 aplikasi ini mempunyai tiga table yaitu :
 - customers = list data customers
 - books = list data buku
 - borrows = data peminjaman buku

sistem ini menggunakan dua RESTAPI yaitu Flask (Customers dan Borrows) dan FastAPI (Books)

sistem ini dilengkapi dengan keamanan JWT sehingga perlu menggunakan access token


