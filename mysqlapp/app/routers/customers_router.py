from app import app
from app.controllers import customers_controller
from app.routers import *
from flask import Blueprint, request
# from app.routers.customers_router import *
# from flask import Flask
# from config import Config
# from flask_jwt_extended import JWTManager

customers_blueprint = Blueprint("customers_router", __name__)

# apps = Flask(__name__)
# apps.config.from_object(Config)
# jwt = JWTManager(apps)

# apps.register_blueprint(customers_blueprint)


@app.route('/users', methods=['GET'])
def showUsers():
    return customers_controller.shows()


@app.route('/user', methods=['GET'])
def showUser():
    params = request.json
    return customers_controller.shows(**params)


@app.route('/user/insert', methods=['POST'])
def insertUsers():
    params = request.json
    return customers_controller.insert(**params)


@app.route('/user/update', methods=['POST'])
def updateUsers():
    params = request.json
    return customers_controller.update(**params)


@app.route('/user/delete', methods=['POST'])
def deleteUsers():
    params = request.json
    return customers_controller.update(**params)


@app.route('/user/requesttoken', methods=['GET'])
def requestToken():
    params = request.json
    return customers_controller.token(**params)
